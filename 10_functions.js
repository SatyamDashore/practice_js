// Functions in JS

function hello(name)
{
    let str = `Hello ${name}!`
    console.log(str);
    return str
}

let greet = hello('Satyam')
console.log(greet);

// Function Expression

let add = function(a,b,c){
    result=a+b+c;
    console.log(a,b,c);
    return result;
}

console.log(add(4,5,6));
console.log(add(4,5));
// JS doesn't give error when less arguments are passed in a function instead assigns undefined

// Arrow functions

let sum = (a,b,c) => a+b+c;

console.log(sum(3,8,2));