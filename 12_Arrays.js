// Arrays in JS. Arrays are also Object type in JS

let values = new Array();
//OR
let arr = [];

console.log(values, typeof values);
console.log(arr, typeof arr);

arr = [2,4,6,1,8];
console.log(arr, typeof arr);
console.log("Length :", arr.length)

arr.push(9)//insert at the end
arr.unshift(12) // inserts at start
arr.pop()  //removes from the end
arr.shift() //removes from the start
console.log(arr, typeof arr);
console.log("Length :", arr.length)


console.log(arr[2]); //will print array element at index 2
console.log(arr[7]); //will print undefined since there are ony 6 elements in the array

// In JS an array can contain different types of data

let data = ["John", 30, {tech:'Python'}, function(){return "Hello!"} ]
console.log(data);
console.log(data[3]());

console.log(arr.push(22))  //This will return the length of the array after push
console.log(arr)
//arr.splice(index,no_of_elements=length-index)
console.log(arr.splice(4))
console.log(arr)
console.log(arr.splice(2,1))
console.log(arr)

//for-of loop with arrays 

console.log("\nfor-of loop with arrays \n");

let nums= [];

nums[0] = 5;
nums[11] = 9;
nums[49] = 20;

console.log(nums);
console.log(nums.length);

for(let n of nums){  
// when using 'of' will iterate through every value in the array
    console.log(n);
}

for(let key in nums){  
    // when using 'in' will iterate through every key(index in terms of array) which have any value
    console.log(key);
}

// Destructuring an Array

let ar = [5,2,8,2,9,1]
console.log(ar);
let [a,b,c,d] = ar;
console.log(a,b,c,d);

//swapping values with array destructuring

[a,b] = [b,a]
console.log(a,b)

// Spliting a string into array

let words = "JS can be used as both client and server side programming language".split(" ");
console.log(words);

// Array forEach method

ar.forEach(n => {
    console.log(n)
})
//console.log(ar);
// Can also pass three values 1st->n, 2nd-> index, 3rd->array
ar.forEach((n,i,nums) => {
    console.log(n,i,nums)
})

// Array method filter map reduce

nums = [42,34,12,41,31,24,11]
console.log(nums.filter(n => n%2===0));
console.log("\n");
nums.filter(n => n%2===0)
.map(n => n*2)
.forEach(n =>{
    console.log(n)
})
console.log("\n");
let total = nums.filter(n => n%2===0)
    .map(n => n*2)
    .reduce((a,b) => a+b) 

console.log(total);