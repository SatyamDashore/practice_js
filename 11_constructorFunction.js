//constructor functions

function Laptop(cpu,ram,brand){
    this.cpu = cpu;
    this.ram = ram;
    this.brand = brand;
    
    this.compare = function(other){
        console.log("This \t | \t Other");
        console.log(`${this.brand} \t | \t ${other.brand}`);
        console.log(`${this.cpu} \t | \t ${other.cpu}`);
        console.log(`${this.ram} \t | \t ${other.ram}`);
    }
}

let laptop1 = new Laptop('i11',"16","MSI");
let laptop2 = new Laptop('m1',"8","Apple");


console.log(laptop1);
console.log(laptop2);

console.log("\nComparision : \n");

laptop1.compare(laptop2)


