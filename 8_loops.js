// Loops in JS

// 1. While loop : intitalization -> condition -> increment
let A=5;
while(A >= 1)
{
    console.log(A);
    A--;
}

// 2. Do While Loop : initalize -> increment -> condition

A=0;
do
{
    console.log(A);
    A--;
}while(A >= 1)

// 3. For Loop : 

for(let i=1;i<=10;i++)
{
    console.log(`10 * ${i} = ${10*i}`);
}