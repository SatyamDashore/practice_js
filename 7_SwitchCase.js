// Switch Case
let grade = 'B';
let marks;
switch (grade)
{
    case 'A':
        marks = 90;
        break;
    case 'B':
        marks = 80;
        break;
    case 'C':
        marks = 70;
        break;
}

//Template Literal

let a = 10
let b = 23
let sum = a + b
console.log(`The sum of ${a} and ${b} is ${sum}`)