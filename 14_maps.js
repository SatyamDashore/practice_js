// Maps in JS is a key value pair collection

let map = new Map();
map.set('Navin','JavaScript')
map.set('Shyam','Java')
map.set('karan','Android')
map.set('Shyam','Java, Python')       // This will update the existing key value pair with the new value

console.log(map.keys());
console.log(map.has('Shyam'));
console.log(map.get('Shyam'));

for(let [k,v] of map)
{
    console.log(k,":",v);
}

map.forEach((v,k) => {
    console.log(k,":",v);
})