// Objects

person = {
    name : {
            fname : "Virat",
            lname : "Kohli"
        },
    age : "33",
    team : ['india', 'RCB']
}

console.log(person)
console.log(typeof person)

console.log(person.name)
console.log(person.name.fname)
console.log(person['age'])
console.log(person.age)

console.log("\nIterating through an object \n")
for(let key in person)
{
    //console.log(key, person.key)  This line will raise error bcoz there is no property named key in the person object
    console.log(key, person[key])
}
delete person.team
console.log(person)

// Methods in Objects

let laptop= {
    cpu : 'i9',
    ram : 8,
    brand : 'hp',

    getConfig : function(){
        console.log(this.cpu, this.ram, this.brand);    //this represents current object
    }
}

laptop.getConfig()
