// Arithmetic Operators  +, -, /, *, %, **

let num1 = true
let num2 = true
let sum = num1 + num2
console.log(sum)

console.log("Square of 6 :", 6 ** 2)
// ++ and -- Increment and Decrement Operators

let x = 4
console.log(x++)
console.log(++x)
console.log(--x)
console.log(x--)

//Realtional Operators <, >, <=, >=, ==, ===

console.log(5 > 6)
console.log(10 > 5)
console.log('Pen' > 'Book')
console.log('Pen'>'Pencil')
console.log("2">1)
console.log(5==5)
console.log("3"==3)

x = ""
let y = false
console.log(x == y)    //Empty string is a falsy value
console.log(x === y)
console.log("3"===3) // Strict Equality Operator

//So when you compare 2 values with == only the data is checked.
//While when comparing with === the data as well as datatype are checked.

// Logical Operators   && , || , !

x = 7
y = 8
let z = 9
console.log(x<y && y>z)
console.log(x<y || y>z)
console.log(! x<y && y>z)

//Bitwise Operators   &, |, ^

console.log(12 & 5)