// Variables: Used to store data temprorily

console.log(2+2)

let a = 5    //Declaring & initializing a Variable
let b = 10
let sum = a + b
console.log(sum)

// JS is a Dynamically typed language so you can change a variables value
a = 9
sum = a + b
console.log(sum)

//Constants can also be declared to avoid dynamic changes in value of a variable
const pi = 3.14
console.log(pi)

// pi = 22 / 7   Will raise error
// TypeError: Assignment to constant variable.

let username = "Satyam"
console.log(username)