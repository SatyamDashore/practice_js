/*
DATA TYPES: It is the type of data which a variable can/has stored.
Types:  1. Primitive - Number, String, Boolean, Null, Undefined, Symbol
        2. Object
*/

let userName = 'Satyam Dashore'     //String
console.log(typeof userName)

let age = 21     //Number
console.log(typeof age)

let DOB;     //Undefined
console.log(typeof DOB)

let result = 20 > 12
console.log(typeof result)

let num = 5/0     // This will assign Infinity to num 
console.log(num)

console.log(Number.MAX_VALUE)
console.log(Number.MAX_VALUE + 10)
console.log(Number.MAX_VALUE * 10)

num = 10210202020202020202020201
console.log(num)  // By assigning a big int to number datatype will cause you losing precision
// Instead we should use bigInt

num = 10210202020202020202020201n
console.log(num)

// Cannot perform arithmetic operations between a number and BigInt 

//console.log(num + 2)   TypeError: Cannot mix BigInt and other types, use explicit conversions
console.log(num + 2n)

let fname = "Satyam"
let lname = "Dashore"

console.log(fname + " " + lname)

//Escape Characters
let title = 'Let\'s Go'
console.log(title)