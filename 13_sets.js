// Sets in JS
// Set is a collection which has all unique values and is unordered

let chrs = new Set("bookkeeper");
console.log(chrs);

chrs.add('s');
chrs.add('k')
console.log(chrs);

chrs.forEach(c => console.log(c))

console.log(chrs.has("p"))