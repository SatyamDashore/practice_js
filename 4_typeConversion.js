//Type Conversion

let num = 6
console.log(num,typeof num)

let pinCode = String(450001)  //number to string
console.log(pinCode, typeof pinCode)

num = Number(true)            //Boolean to Number
console.log(num,typeof num)

console.log(Boolean(null))

console.log(Number("123 Satyam"))

console.log(parseInt("123 Satyam 45"))

//Type Coersion
console.log("\nTYPE COERSION\n")
let x
console.log(x, typeof x)
x = 8
console.log(x, typeof x)
x = x + ""
console.log(x, typeof x)
x = x + 2
console.log(x, typeof x)
x = +x + 8
console.log(x, typeof x)
x = x + ""
console.log(x, typeof x)
x = x - 2 
console.log(x, typeof x)
x = !x
console.log(x, typeof x)